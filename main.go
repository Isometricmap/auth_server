package main

// import "net/http"
import (
	"authserver/router"
	"net/http"
	"authserver/mongoDB"
)

func main() {
	mongoDB.InitMongoConnection("mongodb://localhost:27017/")
	h := router.RegisterHandlers()
	http.ListenAndServe(":8080", h)
}
