package auth

import (
	"bytes"
	"crypto/rand"
	"authserver/mongoDB"
	"io"
	"net/http"
)

func genKey(keySize int) []byte {
	//TODO store key in DB
	key := make([]byte, keySize)
	_, err := rand.Read(key)
	if err != nil {
		return nil
	}
	mongoDB.CreateKey(key)
	return key
}

func verifyKey(apiKey []byte) bool {
	//TODO
	tmpKey := []byte("Bearer bobaloo")
	res := bytes.Compare(tmpKey, apiKey)
	if res == 0 {
		return true
	} else {
		return false
	}
}

func ValidateKey(w http.ResponseWriter, r *http.Request) {
	apiKey := r.Header.Get("Authorization")
	if apiKey == "" || verifyKey([]byte(apiKey)) == false  {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusUnauthorized)
	} 
}

func GetBytes(w http.ResponseWriter, r *http.Request) {
	output := string(genKey(32))
	//error checking
	io.WriteString(w, output)
}


