package router

import (
	"net/http"
	"authserver/auth"
)

func RegisterHandlers() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/test", auth.GetBytes)
	return mux
}
