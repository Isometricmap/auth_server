package mongoDB

import (
	"context"
	"log"
	"fmt"
	"encoding/hex"

	"go.mongodb.org/mongo-driver/bson/primitive"
    "go.mongodb.org/mongo-driver/mongo"
    "go.mongodb.org/mongo-driver/mongo/options"
)

var keyCollection *mongo.Collection
var ctx = context.TODO()


type Key struct {
	ID      primitive.ObjectID	`bson:"_id"`
	apiKey	string				`json: "Field String"`

}

// func (key *Key) newKey(keyValue []byte) {

// }

func CreateKey(key []byte) error {
	mongoEntry := &Key{ID : primitive.NewObjectID(), 
						apiKey: hex.EncodeToString(key)}
	fmt.Println(mongoEntry.apiKey, mongoEntry.ID.Hex())
	_, err := keyCollection.InsertOne(ctx, mongoEntry)
	return err
}

func InitMongoConnection(mongoURI string) {
	clientOptions := options.Client().ApplyURI(mongoURI)
	client_, err := mongo.Connect(ctx, clientOptions)

	if err != nil {
		fmt.Println("Failed to open connection")
		log.Fatal(err)
	}
	keyCollection = client_.Database("apikeys").Collection("keys")
}





